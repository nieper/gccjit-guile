/* Copyright (C) 2019 Marc Nieper-Wißkirchen

   This file is part of gccjit-Guile, Guile bindings for libgccjit.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

// TODO: assert_result (ctxt, XXX) everywhere.
// XXX: scm_remember_upto_here_1 () to be added!

#include <config.h>

#include <libgccjit.h>
#include <libguile.h>
#include <libguile-gccjit.h>
#include <stdio.h>
#include <stdlib.h>

static SCM from_locale_string (char const *str);
static char *to_locale_string (SCM string);
static void array_handle_release (void *handle);
static void assert_result (gcc_jit_context *ctxt, void *result);
static gcc_jit_field **field_array (SCM fields);
static gcc_jit_rvalue **rvalue_array (SCM elements);
static gcc_jit_param **param_array (SCM params);
static gcc_jit_case **case_array (SCM cases);
static void register_weak_reference (SCM from, SCM to);
static void finalize_context (SCM context);
static void finalize_result (SCM result);

SCM_SYMBOL (gccjit, "gccjit");

static SCM weak_refs;

#define DEFINE_SIMPLE_TYPE(type)					\
  static SCM type##_type;						\
									\
  SCM_SNARF_INIT (init_##type##_type ())				\
  static void								\
  init_##type##_type (void)						\
  {									\
    SCM _name = scm_from_utf8_symbol (u8"jit-" #type);			\
    SCM _slots = scm_list_1 (scm_from_utf8_symbol (u8"value"));		\
    scm_t_struct_finalize _finalizer = finalize_##type;			\
    type##_type = scm_make_foreign_object_type (_name, _slots, _finalizer); \
  }									\
									\
  static SCM								\
  make_##type (gcc_jit_##type * value)				\
  {									\
    return scm_make_foreign_object_1 (type##_type, value);		\
  }									\
									\
  static void								\
  assert_##type##_type (SCM obj)					\
  {									\
    scm_assert_foreign_object_type (type##_type, obj);			\
  }									\
									\
  static gcc_jit_##type *						\
  type##_value (SCM obj)						\
  {									\
    return scm_foreign_object_ref (obj, 0);				\
  }									\
									\
  static gcc_jit_##type *						\
  type##_set_value_x (SCM obj, gcc_jit_##type *val)			\
  {									\
    scm_foreign_object_set_x (obj, 0, val);				\
  }

DEFINE_SIMPLE_TYPE (context)
DEFINE_SIMPLE_TYPE (result)

#undef DEFINE_SIMPLE_TYPE

#define DEFINE_OBJECT_TYPE(type)					\
  static SCM type##_type;						\
									\
  SCM_SNARF_INIT (init_##type##_type ())				\
  static void								\
  init_##type##_type (void)						\
  {									\
    SCM _name = scm_from_utf8_symbol (u8"jit-" #type);			\
    SCM _slots = scm_list_2 (scm_from_utf8_symbol (u8"value"),		\
			     scm_from_utf8_symbol (u8"context"));	\
    type##_type = scm_make_foreign_object_type (_name, _slots, NULL);	\
  }									\
									\
  static SCM								\
  make_##type (SCM context, gcc_jit_##type *value)			\
  {									\
    SCM obj = scm_make_foreign_object_1 (type##_type, value);		\
    scm_foreign_object_unsigned_set_x (obj, 1, SCM_UNPACK (context));	\
    return obj;								\
  }									\
									\
  static void								\
  assert_##type##_type (SCM obj)					\
  {									\
    scm_assert_foreign_object_type (type##_type, obj);			\
  }									\
									\
  static gcc_jit_##type *						\
  type##_value (SCM obj)						\
  {									\
    gcc_jit_##type *value = scm_foreign_object_ref (obj, 0);	\
    return value;							\
  }									\
									\
  static SCM								\
  type##_context (SCM obj)						\
  {									\
    SCM context = SCM_PACK (scm_foreign_object_unsigned_ref (obj, 1));	\
    return context;							\
  }

DEFINE_OBJECT_TYPE(object)
DEFINE_OBJECT_TYPE(type)
DEFINE_OBJECT_TYPE(struct)
DEFINE_OBJECT_TYPE(field)
DEFINE_OBJECT_TYPE(function)
DEFINE_OBJECT_TYPE(block)
DEFINE_OBJECT_TYPE(rvalue)
DEFINE_OBJECT_TYPE(lvalue)
DEFINE_OBJECT_TYPE(param)
DEFINE_OBJECT_TYPE(case)

#undef DEFINE_OBJECT_TYPE

static SCM location_type;

SCM_SNARF_INIT (init_location_type ())
static void
init_location_type (void)
{
  SCM _name = scm_from_utf8_symbol (u8"jit-location");
  SCM _slots = scm_list_1 (scm_from_utf8_symbol (u8"value"));
  location_type = scm_make_foreign_object_type (_name, _slots, NULL);
}

static SCM
make_location (SCM context, gcc_jit_location *value)
{
  SCM obj = scm_make_foreign_object_1 (location_type, value);
  register_weak_reference (obj, context);
  return obj;
}

static void
assert_location_type (SCM obj)
{
  if (scm_is_true (obj))
    scm_assert_foreign_object_type (location_type, obj);
}

static gcc_jit_location *
location_value (SCM obj)
{
  if (scm_is_true (obj))
    {
      gcc_jit_location *value = scm_foreign_object_ref (obj, 0);
      return value;
    }
  return NULL;
}

#define DEFINE_UPCAST(from, to, doc)					\
  SCM_DEFINE (scm_jit_##from##_to_##to, "jit-" #from "->" #to, 1, 0, 0,	\
	      (SCM value), doc)						\
  {									\
    assert_##from##_type (value);					\
    gcc_jit_##from * val = from##_value (value);			\
    SCM context = from##_context (value);				\
    return make_##to (context, gcc_jit_##from##_as_##to (val));		\
  }

DEFINE_UPCAST (type, object, "Upcast a type to an object.")
DEFINE_UPCAST (field, object, "Upcast a field to an object.")
DEFINE_UPCAST (rvalue, object, "Upcast an rvalue to an object.")
DEFINE_UPCAST (lvalue, object, "Upcast an lvalue to an object.")
DEFINE_UPCAST (lvalue, rvalue, "Upcast an lvalue to an rvalue.")
DEFINE_UPCAST (param, lvalue, "Upcast a param to an lvalue.")
DEFINE_UPCAST (param, rvalue, "Upcast a param to an rvalue.")
DEFINE_UPCAST (param, object, "Upcast a param to an object.")
DEFINE_UPCAST (function, object, "Upcast a function to an object.")
DEFINE_UPCAST (block, object, "Upcast a block to an object.")
#ifdef LIBGCCJIT_HAVE_SWITCH_STATEMENTS
DEFINE_UPCAST (case, object, "Upcast a case to an object.")
#endif

#undef DEFINE_UPCAST

SCM_DEFINE (scm_jit_make_context, "jit-make-context", 0, 0, 0,
	    (),
	    "Creates a new JIT context.")
{
  gcc_jit_context *ctxt = gcc_jit_context_acquire ();
  assert_result (ctxt, ctxt);
  return make_context (ctxt);
}

SCM_DEFINE (scm_jit_make_child_context, "jit-make-child-context", 1, 0, 0,
	    (SCM parent),
	    "Creates a new child context.")
{
  assert_context_type (parent);
  gcc_jit_context *parent_ctxt = context_value (parent);
  gcc_jit_context *ctxt = gcc_jit_context_new_child_context (parent_ctxt);
  assert_result (parent_ctxt, ctxt);
  SCM child = make_context (ctxt);
  register_weak_reference (child, parent);
  return child;
}

SCM_DEFINE (scm_jit_context_get_first_error, "jit-context-first-error", 1, 0, 0,
	    (SCM context),
	    "Returns the first error message that occurred on the context.")
{
  assert_context_type (context);
  gcc_jit_context *ctxt = context_value (context);
  char const *error = gcc_jit_context_get_first_error (ctxt);
  return from_locale_string (error);
}

SCM_DEFINE (scm_jit_context_dump_to_file, "jit-context-dump-to-file", 3, 0, 0,
	    (SCM context, SCM path, SCM update_locations),
	    "Dumps a C-like representation to the given path.")
{
  assert_context_type (context);
  scm_dynwind_begin (0);
  char *p = scm_to_locale_string (path);
  scm_dynwind_free (p);
  int u = scm_is_true (update_locations);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_context_dump_to_file (ctxt, p, u);
  scm_dynwind_end ();
}

SCM_DEFINE (scm_jit_context_dump_reproducer_to_file,
	    "jit-context-dump-reproducer-to-file",
	    2, 0, 0,
	    (SCM context, SCM path),
	    "Write C source code that will attempt to replay the API.")
{
  assert_context_type (context);
  scm_dynwind_begin (0);
  char *p = scm_to_locale_string (path);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_context_dump_reproducer_to_file (ctxt, p);
  scm_dynwind_free (p);
  scm_dynwind_end ();
}

SCM_DEFINE (scm_jit_context_set_progname_x, "jit-context-set-progname!",
	    2, 0, 0,
	    (SCM context, SCM value),
	    "Sets the name of the program.")
{
  assert_context_type (context);
  scm_dynwind_begin (0);
  char *v = scm_is_false (value) ? NULL : scm_to_locale_string (value);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_context_set_str_option (ctxt, GCC_JIT_STR_OPTION_PROGNAME, v);
  scm_dynwind_free (v);
  scm_dynwind_end ();
}

#define DEFINE_BOOL_OPTION(option, cname, name, doc)			\
  SCM_DEFINE (scm_jit_context_set_##cname##_p_x,			\
	      "jit-context-set-" name "?!",				\
	      2, 0, 0, (SCM context, SCM value), doc)			\
  {									\
    assert_context_type (context);					\
    int v = scm_is_true (value);					\
    gcc_jit_context *ctxt = context_value (context);			\
    gcc_jit_context_set_bool_option (ctxt,				\
				     GCC_JIT_BOOL_OPTION_##option,	\
				     v);				\
    return SCM_UNSPECIFIED;						\
  }

DEFINE_BOOL_OPTION (DEBUGINFO, debug_info, "debug-info",
		    "Enable debugging when set.")
DEFINE_BOOL_OPTION (DUMP_INITIAL_TREE, dump_initial_tree, "dump-initial-tree",
		    "Dump initial tree representation if set.")
DEFINE_BOOL_OPTION (DUMP_INITIAL_GIMPLE, dump_initial_gimple,
		    "dump-initial-gimple",
		    "Dump gimple representation if set.")
DEFINE_BOOL_OPTION (DUMP_GENERATED_CODE, dump_generated_code,
		    "dump-generated-code",
		    "Dump the final generated code if set.")
DEFINE_BOOL_OPTION (DUMP_SUMMARY, dump_summary, "dump-summary",
		    "Print information if set.")
DEFINE_BOOL_OPTION (DUMP_EVERYTHING, dump_everything, "dump-everything",
		    "Dump copious amount of information if set.")
DEFINE_BOOL_OPTION (SELFCHECK_GC, self_check_gc, "self-check-gc",
		    "Aggressively run garbage collector if set.")
DEFINE_BOOL_OPTION (KEEP_INTERMEDIATES, keep_intermediates,
		    "keep-intermediates",
		    "Intermediate files are not cleaned up if set.")

#undef DEFINE_BOOL_OPTION

#define DEFINE_BOOL_OPTION(option, name, doc)				\
  SCM_DEFINE (scm_jit_context_set_bool_##option##_p_x,			\
	      "jit-context-set-" name "?!",				\
	      2, 0, 0, (SCM context, SCM value), doc)			\
  {									\
    assert_context_type (context);					\
    int v = scm_is_true (value);					\
    gcc_jit_context *ctxt = context_value (context);			\
    gcc_jit_context_set_bool_##option (ctxt, v);			\
    return SCM_UNSPECIFIED;						\
  }

#ifdef LIBGCCJIT_HAVE_gcc_jit_context_set_bool_allow_unreachable_blocks
DEFINE_BOOL_OPTION (allow_unreachable_blocks, "allow-unreachable-blocks",
		    "Issue an error about unreachable blocks within a function if set.")
#endif
#ifdef LIBGCCJIT_HAVE_gcc_jit_context_set_bool_use_external_driver
DEFINE_BOOL_OPTION (use_external_driver, "use-external-driver",
		    "Invoke an external driver executable as a subprocess if set.")
#endif

#undef DEFINE_BOOL_OPTION

SCM_DEFINE (scm_jit_context_set_optimization_level_x,
	    "jit-context-set-optimization-level!", 2, 0, 0,
	    (SCM context, SCM value),
	    "How much to optimize the code.")
{
  assert_context_type (context);
  int v = scm_to_int (value);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_context_set_int_option (ctxt, GCC_JIT_INT_OPTION_OPTIMIZATION_LEVEL,
				  v);
  scm_remember_upto_here_1 (context);
  return SCM_UNSPECIFIED;
}

#ifdef LIBGCCJIT_HAVE_gcc_jit_context_add_command_line_option
SCM_DEFINE (scm_jit_context_add_command_line_option_x,
	    "jit-context-add-command-line-option!", 2, 0, 0,
	    (SCM context, SCM option_name),
	    "Add an arbitrary gcc command-line option to the context.")
{
  assert_context_type (context);
  scm_dynwind_begin (0);
  char *o = scm_to_locale_string (option_name);
  scm_dynwind_free (o);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_context_add_command_line_option (ctxt, o);
  scm_dynwind_end ();
}
#endif

SCM_DEFINE (scm_jit_object_context, "jit-object-context", 1, 0, 0,
	    (SCM object),
	    "Return the object's context.")
{
  assert_object_type (object);
  return object_context (object);
}

SCM_DEFINE (scm_jit_object_debug_string, "jit-object-debug-string", 1, 0, 0,
	    (SCM object),
	    "Generate a human-readable description for the given object.")
{
  assert_object_type (object);
  gcc_jit_object *obj = object_value (object);
  char const *s = gcc_jit_object_get_debug_string (obj);
  SCM string = from_locale_string (s);
  return string;
}

#define DEFINE_STANDARD_TYPE(type, cname, name, doc)			\
  SCM_DEFINE (scm_jit_context_type_##cname,				\
	      "jit-context-type-" name,					\
	      1, 0, 0,							\
	      (SCM context),						\
	      doc)							\
  {									\
    assert_context_type (context);					\
    gcc_jit_context *ctxt = context_value (context);			\
    gcc_jit_type *t = gcc_jit_context_get_type (ctxt,			\
						GCC_JIT_TYPE_##type);	\
    SCM _type = make_type (context, t);					\
    return _type;							\
  }

DEFINE_STANDARD_TYPE (VOID, void, "void",
		      "Access void type.")
DEFINE_STANDARD_TYPE (VOID_PTR, void_ptr, "void-ptr",
		      "Access void pointer type.")
DEFINE_STANDARD_TYPE (BOOL, bool, "bool",
		      "Access bool type.")
DEFINE_STANDARD_TYPE (CHAR, char, "char",
		      "Access char type.")
DEFINE_STANDARD_TYPE (SIGNED_CHAR, signed_char, "signed-char",
		      "Access signed char type.")
DEFINE_STANDARD_TYPE (UNSIGNED_CHAR, unsigned_char, "unsigned-char",
		      "Access unsigned char type.")
DEFINE_STANDARD_TYPE (SHORT, short, "short",
		      "Access short type.")
DEFINE_STANDARD_TYPE (UNSIGNED_SHORT, unsigned_short, "unsigned-short",
		      "Access unsigned short type.")
DEFINE_STANDARD_TYPE (INT, int, "int",
		      "Access int type.")
DEFINE_STANDARD_TYPE (UNSIGNED_INT, unsigned_int, "unsigned-int",
		      "Access unsigned int type.")
DEFINE_STANDARD_TYPE (LONG, long, "long",
		      "Access long type.")
DEFINE_STANDARD_TYPE (UNSIGNED_LONG, unsigned_long, "unsigned-long",
		      "Access unsigned long type.")
DEFINE_STANDARD_TYPE (LONG_LONG, long_long, "long-long",
		      "Access long long type.")
DEFINE_STANDARD_TYPE (UNSIGNED_LONG_LONG, unsigned_long_long, "unsigned-long-long",
		      "Access unsigned long long type.")
DEFINE_STANDARD_TYPE (FLOAT, float, "float",
		      "Access float type.")
DEFINE_STANDARD_TYPE (DOUBLE, double, "double",
		      "Access double type.")
DEFINE_STANDARD_TYPE (LONG_DOUBLE, long_double, "long-double",
		      "Access long double type.")
DEFINE_STANDARD_TYPE (CONST_CHAR_PTR, const_char_ptr, "const-char-ptr",
		      "Access const char pointer type.")
DEFINE_STANDARD_TYPE (SIZE_T, size_t, "size-t",
		      "Access to size type.")
DEFINE_STANDARD_TYPE (FILE_PTR, file_ptr, "file-ptr",
		      "Access to file pointer type.")
DEFINE_STANDARD_TYPE (COMPLEX_FLOAT, complex_float, "complex-float",
		      "Access complex float type.")
DEFINE_STANDARD_TYPE (COMPLEX_DOUBLE, complex_double, "complex-double",
		      "Access comples double type.")
DEFINE_STANDARD_TYPE (COMPLEX_LONG_DOUBLE, complex_long_double, "complex-long-double",
		      "Access complex long double type.")

#undef DEFINE_STANDARD_TYPE

SCM_DEFINE (scm_jit_context_int_type, "jit-context-int-type",
	    2, 0, 0,
	    (SCM context, SCM num_bytes, SCM is_signed),
	    "Access the integer type of the given size")
{
  assert_context_type (context);
  int n = scm_to_int (num_bytes);
  int s = scm_is_true (is_signed);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_type *t = gcc_jit_context_get_int_type (ctxt, n, s);
  SCM type = make_type (context, t);
  return type;
}

SCM_DEFINE (scm_jit_typepointer, "jit-type-pointer", 1, 0, 0,
	    (SCM type), "Get pointer to type.")
{
  assert_type_type (type);
  gcc_jit_type *t = type_value (type);
  gcc_jit_type *p = gcc_jit_type_get_pointer (t);
  SCM context = type_context (type);
  SCM pointer_type = make_type (context, p);
  return pointer_type;
}

SCM_DEFINE (scm_jit_type_const, "jit-type-const", 1, 0, 0,
	    (SCM type), "Get const type.")
{
  assert_type_type (type);
  gcc_jit_type *t = type_value (type);
  gcc_jit_type *p = gcc_jit_type_get_const (t);
  SCM context = type_context (type);
  SCM const_type = make_type (context, p);
  return const_type;
}

SCM_DEFINE (scm_jit_type_volatile, "jit-type-volatile", 1, 0, 0,
	    (SCM type), "Get volatile type.")
{
  assert_type_type (type);
  gcc_jit_type *t = type_value (type);
  gcc_jit_type *p = gcc_jit_type_get_volatile (t);
  SCM context = type_context (type);
  SCM volatile_type = make_type (context, p);
  return volatile_type;
}

SCM_DEFINE (scm_jit_context_make_array_type, "jit-context-make-array-type",
	    4, 0, 0,
	    (SCM context, SCM location, SCM element_type, SCM num_elements),
	    "Get array type.")
{
  assert_context_type (context);
  assert_location_type (location);
  assert_type_type (element_type);
  gcc_jit_location *l = location_value (location);
  gcc_jit_type *t = type_value (element_type);
  int n = scm_to_int (num_elements);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_type *a = gcc_jit_context_new_array_type (ctxt, l, t, n);
  SCM array_type = make_type (context, a);
  return array_type;
}

#ifdef LIBGCCJIT_HAVE_gcc_jit_type_get_aligned
SCM_DEFINE (scm_jit_type_aligned, "jit-type-aligned", 2, 0, 0,
	    (SCM type, SCM alignment_in_bytes),
	    "Get aligned type.")
{
  assert_type_type (type);
  gcc_jit_type *t = type_value (type);
  size_t s = scm_to_size_t (alignment_in_bytes);
  gcc_jit_type *a = gcc_jit_type_get_aligned (t, s);
  SCM context = type_context (type);
  SCM aligned_type = make_type (context, a);
  return aligned_type;
}
#endif

#ifdef LIBGCCJIT_HAVE_gcc_jit_type_get_vector
SCM_DEFINE (scm_jit_type_vector, "jit-type-vector", 2, 0, 0,
	    (SCM type, SCM num_units),
	    "Get aligned type.")
{
  assert_type_type (type);
  gcc_jit_type *t = type_value (type);
  size_t s = scm_to_size_t (num_units);
  gcc_jit_type *a = gcc_jit_type_get_vector (t, s);
  SCM context = type_context (type);
  SCM vector_type = make_type (context, a);
  return vector_type;
}
#endif

SCM_DEFINE (scm_jit_context_make_field, "jit-context-make-field", 4, 0, 0,
	    (SCM context, SCM location, SCM type, SCM name),
	    "Create a new field.")
{
  assert_context_type (context);
  assert_location_type (location);
  assert_type_type (type);
  scm_dynwind_begin (0);
  gcc_jit_location *l = location_value (context);
  gcc_jit_type *t = type_value (type);
  char *n = scm_to_locale_string (name);
  scm_dynwind_free (n);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_field *val = gcc_jit_context_new_field (ctxt, l, t, n);
  SCM field = make_field (context, val);
  scm_dynwind_end ();
  return field;
}

SCM_DEFINE (scm_jit_context_make_struct_type, "jit-context-make-struct-type",
	    4, 0, 0,
	    (SCM context, SCM location, SCM name, SCM fields),
	    "Construct a new struct type.")
{
  assert_context_type (context);
  assert_location_type (location);
  scm_dynwind_begin (0);
  gcc_jit_location *l = location_value (location);
  char *n = scm_to_locale_string (name);
  scm_dynwind_free (n);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_field **f = field_array (fields);
  gcc_jit_struct *s =
    gcc_jit_context_new_struct_type (ctxt, l, n,
				     SCM_SIMPLE_VECTOR_LENGTH (fields),
				     f);
  SCM _struct = make_struct (context, s);
  scm_dynwind_end ();
  return _struct;
}

SCM_DEFINE (scm_jit_context_make_opaque_struct,
	    "jit-context-make-opaque-struct", 3, 0, 0,
	    (SCM context, SCM location, SCM name),
	    "Construct an opaque struct type.")
{
  assert_context_type (context);
  assert_location_type (location);
  scm_dynwind_begin (0);
  gcc_jit_location *l = location_value (location);
  char *n = scm_to_locale_string (name);
  scm_dynwind_free (n);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_struct *s = gcc_jit_context_new_opaque_struct (ctxt, l, n);
  SCM _struct = make_struct (context, s);
  scm_dynwind_end ();
  return _struct;
}

SCM_DEFINE (scm_jit_struct_set_fields_x, "jit-struct-set-fields!", 3, 0, 0,
	    (SCM jit_struct, SCM location, SCM fields),
	    "Populate the fields of a formerly-opaque struct type.")
{
  assert_struct_type (jit_struct);
  assert_location_type (location);
  gcc_jit_location *l = location_value (location);
  gcc_jit_field **f = field_array (fields);
  gcc_jit_struct *_struct = struct_value (jit_struct);
  gcc_jit_struct_set_fields (_struct, l, SCM_SIMPLE_VECTOR_LENGTH (fields), f);
  return SCM_UNSPECIFIED;
}

SCM_DEFINE (scm_jit_context_make_union_type, "jit-context-make-union-type",
	    4, 0, 0,
	    (SCM context, SCM location, SCM name, SCM fields),
	    "Construct a new union type.")
{
  assert_context_type (context);
  assert_location_type (location);
  scm_dynwind_begin (0);
  gcc_jit_location *l = location_value (location);
  char *n = scm_to_locale_string (name);
  scm_dynwind_free (n);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_field **f = field_array (fields);
  gcc_jit_type *t =
    gcc_jit_context_new_union_type (ctxt, l, n,
				     SCM_SIMPLE_VECTOR_LENGTH (fields),
				     f);
  SCM type = make_type (context, t);
  scm_dynwind_end ();
  return type;
}

SCM_DEFINE (scm_jit_context_make_int, "jit-context-make-int", 3, 0, 0,
	    (SCM context, SCM numeric_type, SCM value),
	    "Given a numeric type, build an rvalue for the given constant int value.")
{
  assert_context_type (context);
  assert_type_type (numeric_type);
  int val = scm_to_int (value);
  gcc_jit_type *t = type_value (numeric_type);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_rvalue *rval = gcc_jit_context_new_rvalue_from_int (ctxt, t, val);
  SCM rvalue = make_rvalue (context, rval);
  return rvalue;
}

SCM_DEFINE (scm_jit_context_make_long, "jit-context-make-long", 3, 0, 0,
	    (SCM context, SCM numeric_type, SCM value),
	    "Given a numeric type, build an rvalue for the given constant long value.")
{
  assert_context_type (context);
  assert_type_type (numeric_type);
  long val = scm_to_long (value);
  gcc_jit_type *t = type_value (numeric_type);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_rvalue *rval = gcc_jit_context_new_rvalue_from_long (ctxt, t, val);
  SCM rvalue = make_rvalue (context, rval);
  return rvalue;
}

SCM_DEFINE (scm_jit_context_zero, "jit-context-zero", 2, 0, 0,
	    (SCM context, SCM numeric_type),
	    "Given a numeric type, get the rvalue for zero.")
{
  assert_context_type (context);
  assert_type_type (numeric_type);
  gcc_jit_type *t = type_value (numeric_type);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_rvalue *rval = gcc_jit_context_zero (ctxt, t);
  SCM rvalue = make_rvalue (context, rval);
  return rvalue;
}

SCM_DEFINE (scm_jit_context_one, "jit-context-one", 2, 0, 0,
	    (SCM context, SCM numeric_type),
	    "Given a numeric type, get the rvalue for one.")
{
  assert_context_type (context);
  assert_type_type (numeric_type);
  gcc_jit_type *t = type_value (numeric_type);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_rvalue *rval = gcc_jit_context_one (ctxt, t);
  SCM rvalue = make_rvalue (context, rval);
  return rvalue;
}

SCM_DEFINE (scm_jit_context_make_double, "jit-context-make-double", 3, 0, 0,
	    (SCM context, SCM numeric_type, SCM value),
	    "Given a numeric type, build an rvalue for the given constant double value.")
{
  assert_context_type (context);
  assert_type_type (numeric_type);
  double val = scm_to_double (value);
  gcc_jit_type *t = type_value (numeric_type);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_rvalue *rval = gcc_jit_context_new_rvalue_from_double (ctxt, t, val);
  SCM rvalue = make_rvalue (context, rval);
  return rvalue;
}

SCM_DEFINE (scm_jit_context_make_pointer, "jit-context-make-pointer", 3, 0, 0,
	    (SCM context, SCM pointer_type, SCM value),
	    "Given a numeric type, build an rvalue for the given constant pointer value.")
{
  assert_context_type (context);
  assert_type_type (pointer_type);
  void *val = scm_to_pointer (value);
  gcc_jit_type *t = type_value (pointer_type);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_rvalue *rval = gcc_jit_context_new_rvalue_from_ptr (ctxt, t, val);
  SCM rvalue = make_rvalue (context, rval);
  return rvalue;
}

SCM_DEFINE (scm_jit_context_null, "jit-context-null", 2, 0, 0,
	    (SCM context, SCM pointer_type),
	    "Given a numeric type, get the rvalue for NULL.")
{
  assert_context_type (context);
  assert_type_type (pointer_type);
  gcc_jit_type *t = type_value (pointer_type);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_rvalue *rval = gcc_jit_context_null (ctxt, t);
  SCM rvalue = make_rvalue (context, rval);
  return rvalue;
}

SCM_DEFINE (scm_jit_make_string_literal, "jit-make-string-literal", 2, 0, 0,
	    (SCM context, SCM value),
	    "Generate an rvalue for the given string.")
{
  assert_context_type (context);
  scm_dynwind_begin (0);
  char *val = scm_to_locale_string (value);
  scm_dynwind_free (val);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_rvalue *rval = gcc_jit_context_new_string_literal (ctxt, val);
  SCM rvalue = make_rvalue (context, rval);
  scm_dynwind_end ();
  return rvalue;
}

#ifdef LIBGCCJIT_HAVE_gcc_jit_context_new_rvalue_from_vector
SCM_DEFINE (scm_jit_context_make_vector, "jit-context-make-vector", 4, 0, 0,
	    (SCM context, SCM location, SCM vector_type, SCM elements),
	    "Build a vector rvalue from an array of elements.")
{
  assert_context_type (context);
  assert_location_type (location);
  assert_type_type (vector_type);
  gcc_jit_location *l = location_value (location);
  gcc_jit_type *t = type_value (vector_type);
  gcc_jit_rvalue **e = rvalue_array (elements);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_rvalue *rval =
    gcc_jit_context_new_rvalue_from_vector (ctxt, l, t,
					    SCM_SIMPLE_VECTOR_LENGTH (elements),
					    e);
  SCM rvalue = make_rvalue (context, rval);
  return rvalue;
}
#endif

#define DEFINE_UNARY_OP(op, cname, name, doc)				\
  SCM_DEFINE (scm_jit_context_make_unary_op_##cname,			\
	      "jit-context-make-unary-op-" name, 4, 0, 0,		\
	      (SCM context, SCM location, SCM result_type, SCM rvalue),	\
	      doc)							\
  {									\
    assert_context_type (context);					\
    assert_location_type (location);					\
    assert_type_type (result_type);					\
    assert_rvalue_type (rvalue);					\
    gcc_jit_location *l = location_value (location);			\
    gcc_jit_type *t = type_value (result_type);				\
    gcc_jit_rvalue *rval = rvalue_value (rvalue);			\
    gcc_jit_context *ctxt = context_value (context);			\
    rval = gcc_jit_context_new_unary_op (ctxt, l,			\
					 GCC_JIT_UNARY_OP_##op, t,	\
					 rval);				\
    rvalue = make_rvalue (context, rval);				\
    return rvalue;							\
  }

DEFINE_UNARY_OP (MINUS, minus, "minus", "Negate an arithmetic value.")
DEFINE_UNARY_OP (BITWISE_NEGATE, bitwise_negate, "bitwise-negate",
		 "Bitwise negation of an integer value.")
DEFINE_UNARY_OP (LOGICAL_NEGATE, logical_negate, "logical-negate",
		 "Logical negation of an arithmetic or pointer value.")
DEFINE_UNARY_OP (ABS, abs, "abs", "Absolute value of an arithmetic expression.")

#undef DEFINE_UNARY_OP

#define DEFINE_BINARY_OP(op, cname, name, doc)				\
  SCM_DEFINE (scm_jit_context_make_binary_op_##cname,			\
	      "jit-context-make-binary-op-" name, 5, 0, 0,		\
	      (SCM context, SCM location, SCM result_type, SCM a, SCM b), \
	      doc)							\
  {									\
    assert_context_type (context);					\
    assert_location_type (location);					\
    assert_type_type (result_type);					\
    assert_rvalue_type (a);						\
    assert_rvalue_type (b);						\
    gcc_jit_location *l = location_value (location);			\
    gcc_jit_type *t = type_value (result_type);				\
    gcc_jit_rvalue *aval = rvalue_value (a);				\
    gcc_jit_rvalue *bval = rvalue_value (b);				\
    gcc_jit_context *ctxt = context_value (context);			\
    gcc_jit_rvalue *rval						\
      = gcc_jit_context_new_binary_op (ctxt, l,				\
				       GCC_JIT_BINARY_OP_##op, t,	\
				       aval, bval);			\
    SCM rvalue = make_rvalue (context, rval);				\
    return rvalue;							\
  }									\
  SCM_DEFINE (scm_jit_block_add_assignment_##cname##_x,			\
	      "jit-block-add-assignment-" name "!", 4, 0, 0,		\
	      (SCM block, SCM location, SCM lvalue, SCM rvalue),	\
	      "Add evaluation of an rvalue, "				\
	      "using the result to modify an lvalue.")			\
  {									\
    assert_block_type (block);						\
    assert_location_type (location);					\
    assert_lvalue_type (lvalue);					\
    assert_rvalue_type (rvalue);					\
    gcc_jit_block *blk = block_value (block);				\
    gcc_jit_location *loc = location_value (location);			\
    gcc_jit_lvalue *lval = lvalue_value (lvalue);			\
    gcc_jit_rvalue *rval = rvalue_value (rvalue);			\
    gcc_jit_block_add_assignment_op (blk, loc, lval,			\
				     GCC_JIT_BINARY_OP_##op, rval);	\
    scm_remember_upto_here_1 (block);					\
    return SCM_UNSPECIFIED;						\
  }


DEFINE_BINARY_OP(PLUS, plus, "plus", "Addition of arithmetic values.")
DEFINE_BINARY_OP(MINUS, minus, "minus", "Subtraction of arithmetic values.")
DEFINE_BINARY_OP(MULT, mult, "mult",
		 "Multiplication of a pair of arithmetic values.")
DEFINE_BINARY_OP(DIVIDE, divide, "divide",
		 "Quotient of diviion of arithmetic values.")
DEFINE_BINARY_OP(MODULO, modulo, "modulo",
		 "Remainder of division of arithmetic values.")
DEFINE_BINARY_OP(BITWISE_AND, bitwise_and, "bitwise-and", "Bitwise AND.")
DEFINE_BINARY_OP(BITWISE_XOR, bitwise_xor, "bitwise-xor",
		 "Bitwise exclusive OR.")
DEFINE_BINARY_OP(BITWISE_OR, bitwise_or, "bitwise-or",
		 "Bitwise inclusive OR.")
DEFINE_BINARY_OP(LOGICAL_AND, logical_and, "logical-and", "Logical AND.")
DEFINE_BINARY_OP(LOGICAL_OR, logical_or, "logical-or", "Logical OR.")
DEFINE_BINARY_OP(LSHIFT, lshift, "lshift", "Left shift.")
DEFINE_BINARY_OP(RSHIFT, rshift, "rshift", "Right shift.")

#undef DEFINE_BINARY_OP

#define DEFINE_COMPARISON(comparison, name)				\
  SCM_DEFINE (scm_jit_context_make_comparison_##name,			\
	      "jit-context-make-comparison-" #name, 4, 0, 0,		\
	      (SCM context, SCM location, SCM a, SCM b),		\
	      "Build a boolean rvalue out of the comparison of two other rvalues.") \
  {									\
    assert_context_type (context);					\
    assert_location_type (location);					\
    assert_rvalue_type (a);						\
    assert_rvalue_type (b);						\
    gcc_jit_location *l = location_value (location);			\
    gcc_jit_rvalue *aval = rvalue_value (a);				\
    gcc_jit_rvalue *bval = rvalue_value (b);				\
    gcc_jit_context *ctxt = context_value (context);			\
    gcc_jit_rvalue *rval						\
      = gcc_jit_context_new_comparison (ctxt, l,			\
					GCC_JIT_COMPARISON_##comparison, \
					aval, bval);			\
    SCM rvalue = make_rvalue (context, rval);				\
    return rvalue;							\
  }

DEFINE_COMPARISON (EQ, eq)
DEFINE_COMPARISON (NE, ne)
DEFINE_COMPARISON (LT, lt)
DEFINE_COMPARISON (LE, le)
DEFINE_COMPARISON (GT, gt)
DEFINE_COMPARISON (GE, ge)

#undef DEFINE_COMPARISON

SCM_DEFINE (scm_jit_context_make_call, "jit-context-make-call", 4, 0, 0,
	    (SCM context, SCM location, SCM function, SCM args),
	    "Given a function and the given table argument rvalues, "
	    "construct a call to this function, with the result as an rvalue.")
{
  assert_context_type (context);
  assert_location_type (location);
  assert_function_type (function);
  gcc_jit_location *loc = location_value (location);
  gcc_jit_function *func = function_value (function);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_rvalue **a = rvalue_array (args);
  gcc_jit_rvalue *rval
    = gcc_jit_context_new_call (ctxt, loc, func,
				SCM_SIMPLE_VECTOR_LENGTH (args), a);
  SCM rvalue = make_rvalue (context, rval);
  return rvalue;
}


SCM_DEFINE (scm_jit_context_make_call_through_pointer,
	    "jit-context-make-call-through-pointer", 4, 0, 0,
	    (SCM context, SCM location, SCM function_pointer, SCM args),
	    "Given an rvalue of a function pointer type, "
	    "and the given table argument rvalues, "
	    "construct a call to the function pointer, "
	    "with the result as an rvalue.")
{
  assert_context_type (context);
  assert_location_type (location);
  assert_rvalue_type (function_pointer);
  gcc_jit_location *loc = location_value (location);
  gcc_jit_rvalue *fn_ptr = rvalue_value (function_pointer);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_rvalue **a = rvalue_array (args);
  gcc_jit_rvalue *rval
    = gcc_jit_context_new_call_through_ptr (ctxt, loc, fn_ptr,
					    SCM_SIMPLE_VECTOR_LENGTH (args), a);
  SCM rvalue = make_rvalue (context, rval);
  return rvalue;
}

#ifdef LIBGCCJIT_HAVE_gcc_jit_rvalue_set_bool_require_tail_call
SCM_DEFINE (scm_jit_rvalue_set_require_tail_call_p_x,
	    "jit-rvalue-set-require-tail-call?!", 2, 0, 0,
	    (SCM rvalue, SCM require_tail_call_p),
	    "Make/clear the call as needing tail-call optimization.")
{
  assert_rvalue_type (rvalue);
  int flag = scm_is_true (require_tail_call_p);
  gcc_jit_rvalue *rval = rvalue_value (rvalue);
  gcc_jit_rvalue_set_bool_require_tail_call (rval, flag);
  return SCM_UNSPECIFIED;
}
#endif

SCM_DEFINE (scm_jit_context_make_cast, "jit-context-make-cast", 4, 0, 0,
	    (SCM context, SCM location, SCM rvalue, SCM type),
	    "Given an rvalue, construct another rvalue of another type.")
{
  assert_context_type (context);
  assert_location_type (location);
  assert_rvalue_type (rvalue);
  assert_type_type (type);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_location *loc = location_value (location);
  gcc_jit_rvalue *rval = rvalue_value (rvalue);
  gcc_jit_type *t = type_value (type);
  rval = gcc_jit_context_new_cast (ctxt, loc, rval, t);
  rvalue = make_rvalue (context, rval);
  return rvalue;
}

SCM_DEFINE (scm_jit_lvalue_address, "jit-lvalue-address", 2, 0, 0,
	    (SCM lvalue, SCM location),
	    "Take the address of an lvalue.")
{
  assert_lvalue_type (lvalue);
  assert_location_type (location);
  gcc_jit_lvalue *lval = lvalue_value (lvalue);
  gcc_jit_location *loc = location_value (location);
  gcc_jit_rvalue *rval = gcc_jit_lvalue_get_address (lval, loc);
  SCM context = lvalue_context (lvalue);
  SCM rvalue = make_rvalue (context, rval);
  return rvalue;
}

#define DEFINE_MAKE_GLOBAL(kind, name, doc)				\
  SCM_DEFINE (scm_jit_context_make_global_##name,			\
	      "jit-context-make-global-" #name, 4, 0, 0,		\
	      (SCM context, SCM location, SCM type, SCM name),		\
	      doc)							\
  {									\
    assert_context_type (context);					\
    assert_location_type (location);					\
    assert_type_type (type);						\
    scm_dynwind_begin (0);						\
    char *n = scm_to_locale_string (name);				\
    scm_dynwind_free (n);						\
    gcc_jit_location *loc = location_value (location);			\
    gcc_jit_type *t = type_value (type);				\
    gcc_jit_context *ctxt = context_value (context);			\
    gcc_jit_lvalue *lval =						\
      gcc_jit_context_new_global (ctxt, loc, GCC_JIT_GLOBAL_##kind, t, n); \
    SCM lvalue = make_lvalue (context, lval);				\
    scm_dynwind_end ();							\
    return lvalue;							\
  }

DEFINE_MAKE_GLOBAL (EXPORTED, exported, "Add a new exported global variable.")
DEFINE_MAKE_GLOBAL (INTERNAL, internal, "Add a new internal global variable.")
DEFINE_MAKE_GLOBAL (IMPORTED, imported, "Add a new imported global variable.")

#undef DEFINE_MAKE_GLOBAL

SCM_DEFINE (scm_jit_rvalue_dereference, "jit-rvalue-dereference", 2, 0, 0,
	    (SCM rvalue, SCM location),
	    "Given an rvalue of pointer type, dereference the pointer, "
	    "getting an lvalue")
{
  assert_rvalue_type (rvalue);
  assert_location_type (location);
  gcc_jit_rvalue *rval = rvalue_value (rvalue);
  gcc_jit_location *loc= location_value (location);
  gcc_jit_lvalue *lval = gcc_jit_rvalue_dereference (rval, loc);
  SCM context = rvalue_context (rvalue);
  SCM lvalue = make_lvalue (context, lval);
  return lvalue;
}

SCM_DEFINE (scm_jit_lvalue_access_field, "jit-lvalue-access-field", 3, 0, 0,
	    (SCM lvalue, SCM location, SCM field),
	    "Given an lvalue of struct or union type, access the given field.")
{
  assert_lvalue_type (lvalue);
  assert_location_type (location);
  assert_field_type (field);
  gcc_jit_lvalue *lval = lvalue_value (lvalue);
  gcc_jit_location *loc = location_value (location);
  gcc_jit_field *f = field_value (field);
  lval = gcc_jit_lvalue_access_field (lval, loc, f);
  SCM context = lvalue_context (lvalue);
  lvalue = make_lvalue (context, lval);
  return lvalue;
}

SCM_DEFINE (scm_jit_rvalue_access_field, "jit-rvalue-access-field", 3, 0, 0,
	    (SCM rvalue, SCM location, SCM field),
	    "Given an rvalue of struct or union type, access the given field.")
{
  assert_rvalue_type (rvalue);
  assert_location_type (location);
  assert_field_type (field);
  gcc_jit_rvalue *rval = rvalue_value (rvalue);
  gcc_jit_location *loc = location_value (location);
  gcc_jit_field *f = field_value (field);
  rval = gcc_jit_rvalue_access_field (rval, loc, f);
  SCM context = rvalue_context (rvalue);
  rvalue = make_rvalue (context, rval);
  return rvalue;
}

SCM_DEFINE (scm_jit_rvalue_dereference_field, "jit-rvalue-dereference-field",
	    3, 0, 0,
	    (SCM rvalue, SCM location, SCM field),
	    "Given an rvalue of pointer type, "
	    "access the given field as an lvalue.")
{
  assert_rvalue_type (rvalue);
  assert_location_type (location);
  assert_field_type (field);
  gcc_jit_rvalue *rval = rvalue_value (rvalue);
  gcc_jit_location *loc = location_value (location);
  gcc_jit_field *f = field_value (field);
  gcc_jit_lvalue *lval = gcc_jit_rvalue_dereference_field (rval, loc, f);
  SCM context = rvalue_context (rvalue);
  SCM lvalue = make_lvalue (context, lval);
  return lvalue;
}

SCM_DEFINE (scm_jit_context_make_array_access, "jit-context-make-array-access",
	    4, 0, 0,
	    (SCM context, SCM location, SCM pointer, SCM index),
	    "Given an rvalue of pointer type, "
	    "get at the element at the given index.")
{
  assert_context_type (context);
  assert_location_type (location);
  assert_rvalue_type (pointer);
  assert_rvalue_type (index);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_location *loc = location_value (location);
  gcc_jit_rvalue *ptr = rvalue_value (pointer);
  gcc_jit_rvalue *idx = rvalue_value (index);
  gcc_jit_lvalue *lval = gcc_jit_context_new_array_access (ctxt, loc, ptr, idx);
  SCM lvalue = make_lvalue (context, lval);
  return lvalue;
}

SCM_DEFINE (scm_jit_context_make_param, "jit-context-make-param", 4, 0, 0,
	    (SCM context, SCM location, SCM type, SCM name),
	    "Create a new parameter of the given type and name.")
{
  assert_context_type (context);
  assert_location_type (location);
  assert_type_type (type);
  scm_dynwind_begin (0);
  char *n = scm_to_locale_string (name);
  scm_dynwind_free (n);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_location *loc = location_value (location);
  gcc_jit_type *t = type_value (type);
  gcc_jit_param *p = gcc_jit_context_new_param (ctxt, loc, t, n);
  SCM param = make_param (context, p);
  scm_dynwind_end ();
  return param;
}

#define DEFINE_MAKE_FUNCTION(kind, cname, name, doc)			\
  SCM_DEFINE (scm_jit_context_make_function_##cname,			\
	      "jit-context-make-function-" name, 6, 0, 0,		\
	      (SCM context, SCM location, SCM return_type, SCM fname,	\
	       SCM params, SCM variadicp),				\
	      doc)							\
  {									\
    assert_context_type (context);					\
    assert_location_type (location);					\
    assert_type_type (return_type);					\
    scm_dynwind_begin (0);						\
    char *n = scm_to_locale_string (fname);				\
    scm_dynwind_free (n);						\
    gcc_jit_context *ctxt = context_value (context);			\
    gcc_jit_location *loc = location_value (location);			\
    gcc_jit_type *t = type_value (return_type);				\
    gcc_jit_param **p = param_array (params);				\
    int flag = scm_is_true (variadicp);					\
    gcc_jit_function *func =						\
      gcc_jit_context_new_function (ctxt, loc, GCC_JIT_FUNCTION_##kind,	\
				    t, n, SCM_SIMPLE_VECTOR_LENGTH (params), \
				    p, flag);				\
    SCM function = make_function (context, func);			\
    scm_dynwind_end ();							\
    return function;							\
  }

DEFINE_MAKE_FUNCTION (EXPORTED, exported, "exported",
		      "Create an exported function with the given name and parameters.")
DEFINE_MAKE_FUNCTION (INTERNAL, internal, "internal",
		      "Create an internal function with the given name and parameters.")
DEFINE_MAKE_FUNCTION (IMPORTED, imported, "imported",
		      "Create an imported function with the given name and parameters.")
DEFINE_MAKE_FUNCTION (ALWAYS_INLINE, always_inline, "always-inline",
		      "Create an always inlined function with the given name and parameters.")

#undef DEFINE_MAKE_FUNCTION

SCM_DEFINE (scm_jit_context_builtin_function, "jit-context-builtin-function",
	    2, 0, 0,
	    (SCM context, SCM name),
	    "Get builtin function.")
{
  assert_context_type (context);
  scm_dynwind_begin (0);
  char *n = scm_to_locale_string (name);
  scm_dynwind_free (n);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_function *func = gcc_jit_context_get_builtin_function (ctxt, n);
  SCM function = make_function (context, func);
  scm_dynwind_end ();
  return function;
}

SCM_DEFINE (scm_jit_function_param, "jit-function-param", 2, 0, 0,
	    (SCM function, SCM index),
	    "Get the param of the given index.")
{
  assert_function_type (function);
  int idx = scm_to_int (index);
  gcc_jit_function *func = function_value (function);
  gcc_jit_param *p = gcc_jit_function_get_param (func, idx);
  SCM context = function_context (function);
  SCM param = make_param (context, p);
  return param;
}

SCM_DEFINE (scm_jit_function_dump_to_dot, "jit-function-dump-to-dot", 2, 0, 0,
	    (SCM function, SCM path),
	    "Emit the function in graphviz format to the given path.")
{
  assert_function_type (function);
  scm_dynwind_begin (0);
  char *p = scm_to_locale_string (path);
  scm_dynwind_free (p);
  gcc_jit_function *func = function_value (function);
  gcc_jit_function_dump_to_dot (func, p);
  scm_remember_upto_here_1 (function);
  scm_dynwind_end ();
  return SCM_UNSPECIFIED;
}

SCM_DEFINE (scm_jit_function_add_local_x, "jit-function-add-local!", 4, 0, 0,
	    (SCM function, SCM location, SCM type, SCM name),
	    "Create a new local variable within the function.")
{
  assert_function_type (function);
  assert_location_type (location);
  assert_type_type (type);
  scm_dynwind_begin (0);
  char *n = scm_to_locale_string (name);
  scm_dynwind_free (n);
  gcc_jit_function *func = function_value (function);
  gcc_jit_location *loc = location_value (location);
  gcc_jit_type *t = type_value (type);
  gcc_jit_lvalue *lval = gcc_jit_function_new_local (func, loc, t, n);
  SCM context = function_context (function);
  SCM lvalue = make_lvalue (context, lval);
  scm_dynwind_end ();
  return lvalue;
}

SCM_DEFINE (scm_jit_function_add_block_x, "jit-function-add-block!", 2, 0, 0,
	    (SCM function, SCM name),
	    "Create a basic block of the given name.")
{
  assert_function_type (function);
  scm_dynwind_begin (0);
  char *n = to_locale_string (name);
  scm_dynwind_free (n);
  gcc_jit_function *func = function_value (function);
  gcc_jit_block *blk = gcc_jit_function_new_block (func, n);
  SCM context = function_context (function);
  SCM block = make_block (context, blk);
  scm_dynwind_end ();
  return block;
}

SCM_DEFINE (scm_jit_block_add_eval_x, "jit-block-add-eval!", 3, 0, 0,
	    (SCM block, SCM location, SCM rvalue),
	    "Add evaluation of an rvalue.")
{
  assert_block_type (block);
  assert_location_type (location);
  assert_rvalue_type (rvalue);
  gcc_jit_block *blk = block_value (block);
  gcc_jit_location *loc = location_value (location);
  gcc_jit_rvalue *rval = rvalue_value (rvalue);
  gcc_jit_block_add_eval (blk, loc, rval);
  scm_remember_upto_here_1 (block);
  return SCM_UNSPECIFIED;
}

SCM_DEFINE (scm_jit_block_add_assignment_x, "jit-block-add-assignment!",
	    4, 0, 0,
	    (SCM block, SCM location, SCM lvalue, SCM rvalue),
	    "Add evaluation of an rvalue, "
	    "assigning the result to the given lvalue.")
{
  assert_block_type (block);
  assert_location_type (location);
  assert_lvalue_type (lvalue);
  assert_rvalue_type (rvalue);
  gcc_jit_block *blk = block_value (block);
  gcc_jit_location *loc = location_value (location);
  gcc_jit_lvalue *lval = lvalue_value (lvalue);
  gcc_jit_rvalue *rval = rvalue_value (rvalue);
  gcc_jit_block_add_assignment (blk, loc, lval, rval);
  scm_remember_upto_here_1 (block);
  return SCM_UNSPECIFIED;
}

SCM_DEFINE (scm_jit_block_add_comment_x, "jit-block-add-comment!", 3, 0, 0,
	    (SCM block, SCM location, SCM text),
	    "Add a no-op textual commentto the internal representation of the code.")
{
  assert_block_type (block);
  assert_location_type (location);
  scm_dynwind_begin (0);
  char *t = scm_to_locale_string (text);
  scm_dynwind_free (t);
  gcc_jit_block *blk = block_value (block);
  gcc_jit_location *loc = location_value (location);
  gcc_jit_block_add_comment (blk, loc, t);
  scm_remember_upto_here_1 (block);
  scm_dynwind_end ();
  return SCM_UNSPECIFIED;
}

SCM_DEFINE (scm_jit_block_end_with_conditional_x,
	    "jit-block-end-with-conditional!", 5, 0, 0,
	    (SCM block, SCM location, SCM boolean_value, SCM on_true,
	     SCM on_false),
	    "Terminate a block by adding evaluation of an rvalue, "
	    "branching on the result to the appropriate successor block.")
{
  assert_block_type (block);
  assert_location_type (location);
  assert_rvalue_type (boolean_value);
  assert_block_type (on_true);
  assert_block_type (on_false);
  gcc_jit_block *blk = block_value (block);
  gcc_jit_location *loc = location_value (location);
  gcc_jit_rvalue *bool_val = rvalue_value (boolean_value);
  gcc_jit_block *blk1 = block_value (on_true);
  gcc_jit_block *blk2 = block_value (on_false);
  gcc_jit_block_end_with_conditional (blk, loc, bool_val, blk1, blk2);
  scm_remember_upto_here_1 (block);
  return SCM_UNSPECIFIED;
}

SCM_DEFINE (scm_jit_block_end_with_jump_x,
	    "jit-block-end-with-jump!", 3, 0, 0,
	    (SCM block, SCM location, SCM target),
	    "Terminate a block by adding a jump to the given target block.")
{
  assert_block_type (block);
  assert_location_type (location);
  assert_block_type (target);
  gcc_jit_block *blk = block_value (block);
  gcc_jit_location *loc = location_value (location);
  gcc_jit_block *t = block_value (target);
  gcc_jit_block_end_with_jump (blk, loc, t);
  scm_remember_upto_here_1 (block);
  return SCM_UNSPECIFIED;
}

SCM_DEFINE (scm_jit_block_end_with_return_x,
	    "jit-block-end-with-return!", 3, 0, 0,
	    (SCM block, SCM location, SCM rvalue),
	    "Terminate a block by adding evaluation of an rvalue, "
	    "returning the value.")
{
  assert_block_type (block);
  assert_location_type (location);
  assert_rvalue_type (rvalue);
  gcc_jit_block *blk = block_value (block);
  gcc_jit_location *loc = location_value (location);
  gcc_jit_rvalue *rval = rvalue_value (rvalue);
  gcc_jit_block_end_with_return (blk, loc, rval);
  scm_remember_upto_here_1 (block);
  return SCM_UNSPECIFIED;
}

SCM_DEFINE (scm_jit_block_end_with_void_return_x,
	    "jit-block-end-with-void-return!", 2, 0, 0,
	    (SCM block, SCM location),
	    "Terminate a block by adding a valueless return.")
{
  assert_block_type (block);
  assert_location_type (location);
  gcc_jit_block *blk = block_value (block);
  gcc_jit_location *loc = location_value (location);
  gcc_jit_block_end_with_void_return (blk, loc);
  scm_remember_upto_here_1 (block);
  return SCM_UNSPECIFIED;
}

SCM_DEFINE (scm_jit_block_end_with_switch_x,
	    "jit-block-end-with-switch!", 5, 0, 0,
	    (SCM block, SCM location, SCM expression, SCM default_block,
	     SCM cases),
	    "Terminate a block by adding evaluation of an rvalue, "
	    "then performing a multiway branch.")
{
  assert_block_type (block);
  assert_location_type (location);
  assert_rvalue_type (expression);
  assert_block_type (default_block);
  gcc_jit_case **c = case_array (cases);
  gcc_jit_block *blk = block_value (block);
  gcc_jit_location *loc = location_value (location);
  gcc_jit_rvalue *expr = rvalue_value (expression);
  gcc_jit_block *b = block_value (default_block);
  gcc_jit_block_end_with_switch (blk, loc, expr, b,
				 SCM_SIMPLE_VECTOR_LENGTH (cases), c);
  scm_remember_upto_here_1 (block);
  return SCM_UNSPECIFIED;
}

SCM_DEFINE (scm_jit_context_new_case, "jit-context-new-case", 4, 0, 0,
	    (SCM context, SCM min_value, SCM max_value, SCM dest_block),
	    "Create a new case instance for use in a switch statement.")
{
  assert_context_type (context);
  assert_rvalue_type (min_value);
  assert_rvalue_type (max_value);
  assert_block_type (dest_block);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_rvalue *v1 = rvalue_value (min_value);
  gcc_jit_rvalue *v2 = rvalue_value (max_value);
  gcc_jit_block *blk = block_value (dest_block);
  gcc_jit_case *_case = gcc_jit_context_new_case (ctxt, v1, v2, blk);
  SCM jit_case = make_case (context, _case);
  return jit_case;
}

SCM_DEFINE (scm_jit_context_make_location, "jit-context-make-location", 4, 0, 0,
	    (SCM context, SCM filename, SCM line, SCM column),
	    "Create a location instance.")
{
  assert_context_type (context);
  scm_dynwind_begin (0);
  char *f = scm_to_locale_string (filename);
  scm_dynwind_free (f);
  int l = scm_to_int (line);
  int c = scm_to_int (column);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_location *val = gcc_jit_context_new_location (ctxt, f, l, c);
  SCM location = make_location (context, val);
  scm_dynwind_end ();
  return location;
}

/* FIXME: The result of the parent context has to outlive the result
   of the child context. */
// We can do this by changing this into "jit-context-compile-call-with-result...

SCM_DEFINE (scm_jit_context_compile, "jit-context-compile", 1, 0, 0,
	    (SCM context),
	    "Build the code.")
{
  assert_context_type (context);
  gcc_jit_context *ctxt = context_value (context);
  gcc_jit_result *res = gcc_jit_context_compile (ctxt);
  assert_result (ctxt, res);
  SCM result = make_result (res);
  return result;
}

SCM_DEFINE (scm_jit_result_code, "jit-result-code", 2, 0, 0,
	    (SCM result, SCM function_name),
	    "Locate a given function within the built machine code.")
{
  assert_result_type (result);
  scm_dynwind_begin (0);
  char *n = scm_to_locale_string (function_name);
  scm_dynwind_free (n);
  gcc_jit_result *res = result_value (result);
  void *func = gcc_jit_result_get_code (res, n);
  SCM function = scm_from_pointer (func, NULL);
  scm_dynwind_end ();
  return function;
}

SCM_DEFINE (scm_jit_result_global, "jit-result-global", 2, 0, 0,
	    (SCM result, SCM name),
	    "Locate a given global within the built machine code.")
{
  assert_result_type (result);
  scm_dynwind_begin (0);
  char *n = scm_to_locale_string (name);
  scm_dynwind_free (n);
  gcc_jit_result *res = result_value (result);
  void *glob = gcc_jit_result_get_global (res, n);
  SCM global = scm_from_pointer (glob, NULL);
  scm_dynwind_end ();
  return global;
}

SCM_DEFINE (scm_jit_result_release, "jit-result-release", 1, 0, 0,
	    (SCM result),
	    "Unloads the built .so file.")
{
  assert_result_type (result);
  finalize_result (result);
}

#define DEFINE_COMPILER(kind, name, doc)				\
  SCM_DEFINE (scm_jit_context_compile_to_##name,			\
	      "jit-context-compile-to-" #name, 2, 0, 0,			\
	      (SCM context, SCM output_path),				\
	      doc)							\
  {									\
    assert_context_type (context);					\
    scm_dynwind_begin (0);						\
    char *p = scm_to_locale_string (output_path);			\
    scm_dynwind_free (p);						\
    gcc_jit_context *ctxt = context_value (context);			\
    gcc_jit_context_compile_to_file (ctxt, GCC_JIT_OUTPUT_KIND_##kind, p); \
    scm_dynwind_end ();							\
    return SCM_UNSPECIFIED;						\
  }

DEFINE_COMPILER (ASSEMBLER, assembler,
		 "Compile the context to an assembler file.")
DEFINE_COMPILER (OBJECT_FILE, object_file,
		 "Compile the context to an object file.")
DEFINE_COMPILER (DYNAMIC_LIBRARY, dynamic_library,
		 "Compile the context to a dynamic library.")
DEFINE_COMPILER (EXECUTABLE, executable,
		 "Compile the context to an executable file.")

#undef DEFINE_COMPILER

void
scm_gccjit_init (void *ctx)
{
  weak_refs = scm_make_weak_key_hash_table (scm_from_int (42));

# include "gccjit.x"
}

SCM
from_locale_string (char const *str)
{
  return str == NULL ? SCM_BOOL_F : scm_from_locale_string (str);
}

char *
to_locale_string (SCM string)
{
  return scm_is_false (string) ? NULL : scm_to_locale_string (string);
}

void
array_handle_release (void *handle)
{
  scm_array_handle_release (handle);
}

void
assert_result (gcc_jit_context *ctxt, void *result)
{
  if (result != NULL)
    return;
  if (ctxt == NULL)
    scm_throw (gccjit, SCM_BOOL_T);
  char const *error = gcc_jit_context_get_last_error (ctxt);
  scm_throw (gccjit, from_locale_string (error));
}

void
register_weak_reference (SCM from, SCM to)
{
  scm_hashq_set_x (weak_refs, from, to);
}

#define DEFINE_ARRAY(type)						\
  gcc_jit_##type **							\
  type##_array (SCM elts)						\
  {									\
    size_t n = SCM_SIMPLE_VECTOR_LENGTH (elts);				\
    gcc_jit_##type **e =						\
      scm_gc_malloc_pointerless (n * sizeof (gcc_jit_##type *), NULL);	\
    scm_t_array_handle handle;						\
    size_t i, len;							\
    ssize_t inc;							\
    SCM const *elt;							\
    scm_dynwind_begin (0);						\
    elt = scm_vector_elements (elts, &handle, &len, &inc);		\
    scm_dynwind_unwind_handler (array_handle_release, &handle,		\
				SCM_F_WIND_EXPLICITLY);			\
    for (i = 0; i < len; i++, elt += inc)				\
      {									\
	assert_##type##_type (*elt);					\
	e [i] = type##_value (*elt);					\
      }									\
    scm_dynwind_end ();							\
    return e;								\
  }

DEFINE_ARRAY (field)
DEFINE_ARRAY (rvalue)
DEFINE_ARRAY (param)
DEFINE_ARRAY (case)

#undef DEFINE_ARRAY

void
finalize_context (SCM context)
{
  gcc_jit_context *ctxt = context_value (context);
  if (ctxt != NULL)
    {
      gcc_jit_context_release (ctxt);
      context_set_value_x (context, NULL);
    }
}

void
finalize_result (SCM result)
{
  gcc_jit_result *res = result_value (result);
  if (res != NULL)
    {
      gcc_jit_result_release (res);
      result_set_value_x (result, NULL);
    }
}
