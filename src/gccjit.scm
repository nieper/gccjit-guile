;; Copyright (C) 2019 Marc Nieper-Wißkirchen

;; This file is part of gccjit-Guile, Guile bindings for libgccjit.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gccjit)
  #:use-module (srfi srfi-11)
  #:export (jit-make-context
	    jit-make-child-context
	    jit-context-first-error
	    jit-context-dump-to-file
	    jit-context-dump-reproducer-to-file
	    jit-context-set-program-name!
	    jit-context-set-debug-info?!
	    jit-context-set-dump-initial-tree?!
	    jit-context-set-dump-initial-gimple?!
	    jit-context-set-dump-generated-code?!
	    jit-context-set-summary?!
	    jit-context-set-dump-everything?!
	    jit-context-set-self-check-gc?!
	    jit-context-set-keep-intermediates?!
	    jit-context-set-allow-unreachable-blocks?!
	    jit-context-set-use-external-driver?!
	    jit-context-set-optimization-level!
	    jit-context-add-command-line-option!
	    jit-object-context
	    jit-object-debug-string

	    jit-context-type-void
	    jit-context-type-void-ptr
	    jit-context-type-bool
	    jit-context-type-char
	    jit-context-type-signed-char
	    jit-context-type-unsigned-char
	    jit-context-type-short
	    jit-context-type-unsigned-char
	    jit-context-type-short
	    jit-context-type-unsigned-short
	    jit-context-type-int
	    jit-context-type-unsigned-int
	    jit-context-type-long
	    jit-context-type-unsigned-long
	    jit-context-type-long-long
	    jit-context-type-unsigned-long-long
	    jit-context-type-float
	    jit-context-type-double
	    jit-context-type-long-double
	    jit-context-type-const-char-ptr
	    jit-context-type-size-t
	    jit-context-type-file-ptr
	    jit-context-type-complex-float
	    jit-context-type-complex-double
	    jit-context-type-complex-long-double
	    jit-context-int-type
	    jit-type-pointer
	    jit-type-const
	    jit-type-volatile
	    jit-type-aligned
	    jit-context-make-array-type
	    jit-type-vector
	    jit-context-make-field
	    jit-context-make-struct-type
	    jit-context-make-opaque-struct
	    jit-struct-set-fields!
	    jit-context-make-union-type

	    jit-context-make-int
	    jit-context-make-long
	    jit-context-zero
	    jit-context-one
	    jit-context-make-double
	    jit-context-make-pointer
	    jit-context-make-null
	    jit-context-make-string-literal
	    jit-context-make-vector
	    jit-context-make-unary-op-minus
	    jit-context-make-unary-op-bitwise-negate
	    jit-context-make-unary-op-logical-negate
	    jit-context-make-unary-op-abs
	    jit-context-make-binary-op-plus
	    jit-context-make-binary-op-minus
	    jit-context-make-binary-op-mult
	    jit-context-make-binary-op-divide
	    jit-context-make-binary-op-modulo
	    jit-context-make-binary-op-bitwise-and
	    jit-context-make-binary-op-bitwise-xor
	    jit-context-make-binary-op-bitwise-or
	    jit-context-make-binary-op-logical-and
	    jit-context-make-binary-op-logical-or
	    jit-context-make-binary-op-lshift
	    jit-context-make-binary-op-rshift
	    jit-context-make-comparison-eq
	    jit-context-make-comparison-ne
	    jit-context-make-comparison-lt
	    jit-context-make-comparison-le
	    jit-context-make-comparison-gt
	    jit-context-make-comparison-gt
	    jit-context-make-call
	    jit-context-make-call-through-pointer
	    jit-rvalue-set-require-tail-call?!
	    jit-context-make-cast
	    jit-lvalue-address
	    jit-context-make-global-exported
	    jit-context-make-global-internal
	    jit-context-make-global-imported
	    jit-rvalue-dereference
	    jit-lvalue-access-field
	    jit-rvalue-access-field
	    jit-rvalue-dereference-field
	    jit-context-make-array-access
	    jit-context-make-param
	    jit-context-make-function-exported
	    jit-context-make-function-internal
	    jit-context-make-function-imported
	    jit-context-make-function-always-inline
	    jit-context-builtin-function
	    jit-function-param
	    jit-function-dump-to-dot
	    jit-function-add-local!
	    jit-function-add-block!
	    jit-block-function
	    jit-block-add-eval!
	    jit-block-add-assignment!
	    jit-block-add-assignment-plus!
	    jit-block-add-assignment-minus!
	    jit-block-add-assignment-mult!
	    jit-block-add-assignment-divide!
	    jit-block-add-assignment-modulo!
	    jit-block-add-assignment-bitwise-and!
	    jit-block-add-assignment-bitwise-xor!
	    jit-block-add-assignment-bitwise-or!
	    jit-block-add-assignment-logical-and!
	    jit-block-add-assignment-logical-or!
	    jit-block-add-assignment-lshift!
	    jit-block-add-assignment-rshift!
	    jit-block-add-comment!
	    jit-block-end-with-conditional!
	    jit-block-end-with-jump!
	    jit-block-end-with-return!
	    jit-block-end-with-void-return!
	    jit-block-end-with-switch
	    jit-function-address
	    jit-context-make-function-pointer-type

	    jit-context-make-location

	    jit-context-compile
	    jit-result-code
	    jit-result-global
	    jit-result-release
	    jit-context-compile-to-assembler
	    jit-context-compile-to-object-file
	    jit-context-compile-to-dynamic-library
	    jit-context-compile-to-executable

	    jit-type->object
	    jit-field->object
	    jit-rvalue->object
	    jit-lvalue->object
	    jit-lvalue->rvalue
	    jit-param->lvalue
	    jit-param->rvalue
	    jit-param->object
	    jit-function->object
	    jit-block->object
	    jit-case->object

	    jit-call-with-result))

(load-extension "libguile-gccjit" "scm_gccjit_init")

(define (jit-call-with-result context proc)
  (let ((guardian (make-guardian))
	(result (jit-context-compile context)))
    (guardian result)
    (let*-values ((res (proc result)))
      (guardian)
      (apply values res))))
