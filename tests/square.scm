;; Copyright (C) 2019 Marc Nieper-Wißkirchen

;; This file is part of gccjit-Guile, Guile bindings for libgccjit.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (srfi srfi-64)
	     (system foreign)
	     (gccjit))

(test-begin "Trivial machine code function")

(define (create-code context)
  (define int-type
    (jit-context-type-int context))
  (define param/i
    (jit-context-make-param context #f int-type "i"))
  (define func
    (jit-context-make-function-exported context
					#f
					int-type
					"square"
					(vector param/i)
					#f))
  (define block
    (jit-function-add-block! func #f))
  (define expr
    (jit-context-make-binary-op-mult context
				     #f
				     int-type
				     (jit-param->rvalue param/i)
				     (jit-param->rvalue param/i)))
  (jit-block-end-with-return! block #f expr))

(define context (jit-make-context))
(create-code context)

(test-equal "square" 25
            (jit-call-with-result context
              (lambda (result)
                (let* ((csquare (jit-result-code result "square"))
                       (square (pointer->procedure int csquare (list int))))
                  (square 5)))))

(test-end)
