This file attempts to describe the rules to use when hacking gccjit-Guile.
Don't put this file into the distribution.

Everything related to the development of gccjit-Guile is on Gitlab:

        https://gitlab.com/nieper/gccjit-guile


* Administrativa

** If you incorporate a change from somebody on the net:
Be sure to add their name and email address to THANKS.

** If a change fixes a test, mention the test in the commit message.

** Bug reports
If somebody reports a new bug, mention their name in the commit message and in
the test case you write.  Put them into THANKS.

The correct response to most actual bugs is to write a new test case which
demonstrates the bug.  Then fix the bug, re-run the test suite, and check
everything in.


* Hacking

** Visible changes
Which include serious bug fixes, must be mentioned in NEWS.


* Working from the repository

These notes intend to help people working on the checked-out sources.  These
requirements do not apply when building from a distribution tarball.

** Requirements
We've opted to keep only the highest-level sources in the repository.  This
eases our maintenance burden, (fewer merges etc.), but imposes more
requirements on anyone wishing to build from the just-checked-out sources.
For example, you have to use the latest stable versions of the maintainer
tools we depend upon, including:

- Autoconf <https://www.gnu.org/software/autoconf/>
- Automake <https://www.gnu.org/software/automake/>
- Git-Merge-Changelog <https://www.gnu.org/software/gnulib/>
- Gzip <https://www.gnu.org/software/gzip/>
- Perl <https://www.cpan.org/>
- Rsync <https://samba.anu.edu.au/rsync/>
- Tar <https://www.gnu.org/software/tar/>
- Texinfo <https://www.gnu.org/software/texinfo/>

Valgrind <http://valgrind.org/> is also highly recommended, if it supports
your architecture.

If you're using a GNU/Linux distribution, the easiest way to install the
above packages depends on your system.  The following shell command should
work for Debian-based systems such as Ubuntu:

  sudo apt install \
    autoconf automake git git-merge-changelog texinfo valgrind

** First checkout

Obviously, if you are reading these notes, you did manage to check out this
package from the repository.

The first steps are just

        $ ./bootstrap
        $ ./configure
        $ make
        $ make check

At this point, there should be no difference between your local copy, and
the master copy:

        $ git diff

should output no difference.

Enjoy!


** Updating

*** Updating to a newer version of gnulib

Youcan use this sequence to update to a newer version of gnulib:

        $ git submodule foreach git pull origin master
        $ git add .gnulib
        $ ./bootstrap

Make sure gccjit-Guile can live with that version of gnulib.

        $ make distcheck


* Test suite

** make check
Use liberally.


* Release Procedure
See the file README-release.

-----

Copyright (C) 2019 Marc Nieper-Wißkirchen

This file is part of gccjit-Guile, Guile bindings for libgccjit.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Local Variables:
mode: outline
fill-column: 76
End:
