/* Copyright (C) 2019 Marc Nieper-Wißkirchen

   This file is part of gccjit-Guile, Guile bindings for libgccjit.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef LIBGUILE_GCCJIT_H_INCLUDED
#define LIBGUILE_GCCJIT_H_INCLUDED

#include <libgccjit.h>
#include <libguile.h>

SCM scm_jit_make_context (void);
SCM scm_jit_make_child_context (SCM context);
SCM scm_jit_context_first_error (SCM context);
SCM scm_jit_context_dump_to_file (SCM context, SCM path,
				  SCM update_locations_p);
SCM scm_jit_context_dump_reproducer_to_file (SCM context, SCM path);
SCM scm_jit_context_set_program_name_x (SCM context, SCM value);
SCM scm_jit_context_set_debug_info_p_x (SCM context, SCM value);
SCM scm_jit_context_set_dump_initial_tree_p_x (SCM context, SCM value);
SCM scm_jit_context_set_dump_initial_gimple_p_x (SCM context, SCM value);
SCM scm_jit_context_set_dump_generated_code_p_x (SCM context, SCM value);
SCM scm_jit_context_set_dump_summary_p_x (SCM context, SCM value);
SCM scm_jit_context_set_dump_everything_p_x (SCM context, SCM value);
SCM scm_jit_context_set_self_check_gc_p_x (SCM context, SCM value);
SCM scm_jit_context_set_keep_intermediates_p_x (SCM context, SCM value);
#ifdef LIBGCCJIT_HAVE_gcc_jit_context_set_bool_allow_unreachable_blocks
SCM scm_jit_context_set_allow_unreachable_blocks_p_x (SCM context, SCM value);
#endif
#ifdef LIBGCCJIT_HAVE_gcc_jit_context_set_bool_use_external_driver
SCM scm_jit_context_set_use_external_driver_p_x (SCM context, SCM value);
#endif
SCM scm_jit_context_set_optimization_level_x (SCM context, SCM value);
#ifdef LIBGCCJIT_HAVE_gcc_jit_context_add_command_line_option
SCM scm_jit_context_add_command_line_option_x (SCM context, SCM option_name);
#endif

SCM scm_jit_object_context (SCM object);
SCM scm_jit_object_debug_string (SCM object);

SCM scm_jit_context_type_void (SCM context);
SCM scm_jit_context_type_void_ptr (SCM context);
SCM scm_jit_context_type_bool (SCM context);
SCM scm_jit_context_type_char (SCM context);
SCM scm_jit_context_type_signed_char (SCM context);
SCM scm_jit_context_type_unsigned_char (SCM context);
SCM scm_jit_context_type_short (SCM context);
SCM scm_jit_context_type_unsigned_short (SCM context);
SCM scm_jit_context_type_int (SCM context);
SCM scm_jit_context_type_unsigned_int (SCM context);
SCM scm_jit_context_type_long (SCM context);
SCM scm_jit_context_type_unsigned_long (SCM context);
SCM scm_jit_context_type_long_long (SCM context);
SCM scm_jit_context_type_unsigned_long_long (SCM context);
SCM scm_jit_context_type_float (SCM context);
SCM scm_jit_context_type_double (SCM context);
SCM scm_jit_context_type_long_double (SCM context);
SCM scm_jit_context_type_const_char_ptr (SCM context);
SCM scm_jit_context_type_size_t (SCM context);
SCM scm_jit_context_type_file_ptr (SCM context);
SCM scm_jit_context_type_complex_float (SCM context);
SCM scm_jit_context_type_complex_double (SCM context);
SCM scm_jit_context_type_complex_long_double (SCM context);
SCM scm_jit_context_int_type (SCM context, SCM bytes, SCM signed_p);
SCM scm_jit_type_pointer (SCM type);
SCM scm_jit_type_const (SCM type);
SCM scm_jit_type_volatile (SCM type);
SCM scm_jit_context_make_array_type (SCM context, SCM location,
				     SCM element_type, SCM num_elements);
#ifdef LIBGCCJIT_HAVE_gcc_jit_type_get_aligned
SCM scm_jit_type_aligned (SCM type, SCM alignment_in_bytes);
#endif
#ifdef LIBGCCJIT_HAVE_gcc_jit_type_get_vector
SCM scm_jit_type_vector (SCM type, SCM num_units);
#endif
SCM scm_jit_context_make_field (SCM context, SCM location, SCM type, SCM name);
SCM scm_jit_context_make_struct_type (SCM context, SCM location, SCM name,
				      SCM fields);
SCM scm_jit_context_make_opaque_struct (SCM context, SCM location, SCM name);
SCM scm_jit_struct_set_fields_x (SCM struct_, SCM location, SCM fields);
SCM scm_jit_context_make_union_type (SCM context, SCM location, SCM name,
				     SCM fields);

SCM scm_jit_context_make_int (SCM context, SCM numeric_type, SCM value);
SCM scm_jit_context_make_long (SCM context, SCM numeric_type, SCM value);
SCM scm_jit_context_zero (SCM context, SCM numeric_type);
SCM scm_jit_context_one (SCM context, SCM numeric_type);
SCM scm_jit_context_make_double (SCM context, SCM numeric_type, SCM value);
SCM scm_jit_context_make_pointer (SCM context, SCM pointer_type, SCM value);
SCM scm_jit_context_null (SCM context, SCM pointer_type);
SCM scm_jit_context_make_string_literal (SCM context, SCM value);
#ifdef LIBGCCJIT_HAVE_gcc_jit_context_new_rvalue_from_vector
SCM scm_jit_context_make_vector (SCM context, SCM location, SCM vector_type,
				 SCM elements);
#endif
SCM scm_jit_context_make_unary_op_minus (SCM context, SCM location,
					 SCM result_type, SCM rvalue);
SCM scm_jit_context_make_unary_op_bitwise_negate (SCM context, SCM location,
						  SCM result_type, SCM rvalue);
SCM scm_jit_context_make_unary_op_logical_negate (SCM context, SCM location,
						  SCM result_type, SCM rvalue);
SCM scm_jit_context_make_unary_op_abs (SCM context, SCM location,
				       SCM result_type, SCM rvalue);
SCM scm_jit_context_make_binary_op_plus (SCM context, SCM location,
					 SCM result_type, SCM a, SCM b);
SCM scm_jit_context_make_binary_op_minus (SCM context, SCM location,
					  SCM result_type, SCM a, SCM b);
SCM scm_jit_context_make_binary_op_mult (SCM context, SCM location,
					 SCM result_type, SCM a, SCM b);
SCM scm_jit_context_make_binary_op_divide (SCM context, SCM location,
					   SCM result_type, SCM a, SCM b);
SCM scm_jit_context_make_binary_op_modulo (SCM context, SCM location,
					   SCM result_type, SCM a, SCM b);
SCM scm_jit_context_make_binary_op_bitwise_and (SCM context, SCM location,
						SCM result_type, SCM a, SCM b);
SCM scm_jit_context_make_binary_op_bitwise_xor (SCM context, SCM location,
						SCM result_type, SCM a, SCM b);
SCM scm_jit_context_make_binary_op_bitwise_or (SCM context, SCM location,
					       SCM result_type, SCM a, SCM b);
SCM scm_jit_context_make_binary_op_logical_and (SCM context, SCM location,
						SCM result_type, SCM a, SCM b);
SCM scm_jit_context_make_binary_op_logical_or (SCM context, SCM location,
					       SCM result_type, SCM a, SCM b);
SCM scm_jit_context_make_binary_op_lshift (SCM context, SCM location,
					   SCM result_type, SCM a, SCM b);
SCM scm_jit_context_make_binary_op_rshift (SCM context, SCM location,
					   SCM result_type, SCM a, SCM b);
SCM scm_jit_context_make_comparison_eq (SCM context, SCM location, SCM a,
					SCM b);
SCM scm_jit_context_make_comparison_ne (SCM context, SCM location, SCM a,
					SCM b);
SCM scm_jit_context_make_comparison_lt (SCM context, SCM location, SCM a,
					SCM b);
SCM scm_jit_context_make_comparison_le (SCM context, SCM location, SCM a,
					SCM b);
SCM scm_jit_context_make_comparison_gt (SCM context, SCM location, SCM a,
					SCM b);
SCM scm_jit_context_make_comparison_ge (SCM context, SCM location, SCM a,
					SCM b);
SCM scm_jit_context_make_call (SCM context, SCM location, SCM function,
			       SCM args);
SCM scm_jit_context_make_call_through_pointer (SCM context, SCM location,
					       SCM function_pointer, SCM args);
#ifdef LIBGCCJIT_HAVE_gcc_jit_rvalue_set_bool_require_tail_call
SCM scm_jit_rvalue_set_require_tail_call_p_x (SCM call,
					      SCM require_tail_call_p);
#endif
SCM scm_jit_context_make_cast (SCM context, SCM location, SCM rvalue, SCM type);
SCM scm_jit_lvalue_address (SCM lvalue, SCM location);
SCM scm_jit_context_make_global_exported (SCM context, SCM location, SCM type,
					  SCM name);
SCM scm_jit_context_make_global_internal (SCM context, SCM location, SCM type,
					  SCM name);
SCM scm_jit_context_make_global_imported (SCM context, SCM location, SCM type,
					  SCM name);
SCM scm_jit_rvalue_dereference (SCM rvalue, SCM location);
SCM scm_jit_lvalue_access_field (SCM lvalue, SCM location, SCM field);
SCM scm_jit_rvalue_access_field (SCM rvalue, SCM location, SCM field);
SCM scm_jit_rvalue_dereference_field (SCM pointer, SCM location, SCM field);
SCM scm_jit_context_make_array_access (SCM context, SCM location, SCM pointer,
				       SCM index);

SCM scm_jit_context_make_param (SCM context, SCM location, SCM type, SCM name);
SCM scm_jit_context_make_function_exported (SCM context, SCM location,
					    SCM return_type, SCM name,
					    SCM params, SCM variadicp);
SCM scm_jit_context_make_function_internal (SCM context, SCM location,
					    SCM return_type, SCM name,
					    SCM params, SCM variadicp);
SCM scm_jit_context_make_function_imported (SCM context, SCM location,
					    SCM return_type, SCM name,
					    SCM params, SCM variadicp);
SCM scm_jit_context_make_function_always_inline (SCM context, SCM location,
						 SCM return_type, SCM name,
						 SCM params, SCM variadicp);
SCM scm_jit_context_builtin_function (SCM context, SCM name);
SCM scm_jit_function_param (SCM function, SCM index);
SCM scm_jit_function_dump_to_dot (SCM function, SCM path);
SCM scm_jit_function_add_local_x (SCM function, SCM location, SCM type,
				  SCM name);
SCM scm_jit_function_add_block_x (SCM function, SCM name);
SCM scm_jit_block_function (SCM block);
SCM scm_jit_block_add_eval_x (SCM block, SCM location, SCM rvalue);
SCM scm_jit_block_add_assignment_x (SCM block, SCM location, SCM lvalue,
				    SCM rvalue);
SCM scm_jit_block_add_assignment_plus_x (SCM block, SCM location, SCM lvalue,
					 SCM rvalue);
SCM scm_jit_block_add_assignment_minus_x (SCM block, SCM location, SCM lvalue,
					  SCM rvalue);
SCM scm_jit_block_add_assignment_mult_x (SCM block, SCM location, SCM lvalue,
					 SCM rvalue);
SCM scm_jit_block_add_assignment_divide_x (SCM block, SCM location, SCM lvalue,
					   SCM rvalue);
SCM scm_jit_block_add_assignment_modulo_x (SCM block, SCM location, SCM lvalue,
					   SCM rvalue);
SCM scm_jit_block_add_assignment_bitwise_and_x (SCM block, SCM location,
						SCM lvalue, SCM rvalue);
SCM scm_jit_block_add_assignment_bitwise_xor_x (SCM block, SCM location,
						SCM lvalue, SCM rvalue);
SCM scm_jit_block_add_assignment_bitwise_or_x (SCM block, SCM location,
					       SCM lvalue, SCM rvalue);
SCM scm_jit_block_add_assignment_logical_and_x (SCM block, SCM location,
						SCM lvalue, SCM rvalue);
SCM scm_jit_block_add_assignment_logical_or_x (SCM block, SCM location,
					       SCM lvalue, SCM rvalue);
SCM scm_jit_block_add_assignment_lshift_x (SCM block, SCM location,
					   SCM lvalue, SCM rvalue);
SCM scm_jit_block_add_assignment_rshift_x (SCM block, SCM location,
					   SCM lvalue, SCM rvalue);
SCM scm_jit_block_add_comment_x (SCM block, SCM location, SCM text);
SCM scm_jit_block_end_with_conditional_x (SCM block, SCM location,
					  SCM boolean_value, SCM on_true,
					  SCM on_false);
SCM scm_jit_block_end_with_jump_x (SCM block, SCM location, SCM target);
SCM scm_jit_block_end_with_return_x (SCM block, SCM location, SCM rvalue);
SCM scm_jit_block_end_with_void_return_x (SCM block, SCM location);
#ifdef LIBGCCJIT_HAVE_SWITCH_STATEMENTS
SCM scm_jit_block_end_with_switch_x (SCM block, SCM location, SCM expr,
				     SCM default_block, SCM cases);
SCM scm_jit_context_make_case (SCM context, SCM min_value, SCM max_value,
			       SCM dest_block);
#endif

#ifdef LIBGCCJIT_HAVE_gcc_jit_function_get_address
SCM scm_jit_function_address (SCM function, SCM location);
#endif
SCM scm_jit_context_make_function_pointer_type (SCM context, SCM location,
						SCM return_type,
						SCM param_types,
						SCM variadicp);

SCM scm_jit_context_make_location (SCM context, SCM filename, SCM line,
				   SCM column);

SCM scm_jit_context_compile (SCM context);
SCM scm_jit_result_code (SCM result, SCM function_name);
SCM scm_jit_result_global (SCM result, SCM name);
SCM scm_jit_result_release (SCM result);
SCM scm_jit_context_compile_to_assembler (SCM context, SCM output_path);
SCM scm_jit_context_compile_to_object_file (SCM context, SCM output_path);
SCM scm_jit_context_compile_to_dynamic_library (SCM context, SCM output_path);
SCM scm_jit_context_compile_to_executable (SCM context, SCM output_path);

SCM scm_jit_type_to_object (SCM value);
SCM scm_jit_field_to_object (SCM value);
SCM scm_jit_rvalue_to_object (SCM value);
SCM scm_jit_lvalue_to_object (SCM value);
SCM scm_jit_lvalue_to_rvalue (SCM value);
SCM scm_jit_param_to_lvalue (SCM value);
SCM scm_jit_param_to_rvalue (SCM value);
SCM scm_jit_param_to_object (SCM value);
SCM scm_jit_function_to_object (SCM value);
SCM scm_jit_block_to_object (SCM value);
#ifdef LIBGCCJIT_HAVE_SWITCH_STATEMENTS
SCM scm_jit_case_to_object (SCM value);
#endif

void scm_gccjit_init (void *ctx);

#endif /* LIBGUILE_GCCJIT_H_INCLUDED */
