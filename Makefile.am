# Copyright (C) 2019 Marc Nieper-Wißkirchen.

# This file is part of gccgit-Guile, Guile bindings for libgccjit.

# gccgit-Guile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

ACLOCAL_AMFLAGS = -I m4

SUBDIRS = lib include src doc tests

EXTRA_DIST = m4/gnulib-cache.m4 $(top_srcdir)/cfg.mk		\
             $(top_srcdir)/.prev-version $(top_srcdir)/.version

BUILT_SOURCES = $(top_srcdir)/.version

$(top_srcdir)/.version:
	echo $(VERSION) > $@-t && mv $@-t $@
dist-hook: gen-ChangeLog
	echo $(VERSION) > $(distdir)/.tarball-version

gen_start_date = 2019-02-01
.PHONY: gen-ChangeLog
gen-ChangeLog:
	$(AM_V_GEN)if test -d $(srcdir)/.git; then	\
	  cl=$(distdir)/ChangeLog &&			\
	  rm -f $$cl.tmp &&				\
	  $(top_srcdir)/build-aux/gitlog-to-changelog	\
	    --strip-tab					\
	    --strip-cherry-pick				\
	    --no-cluster				\
	    --amend=$(srcdir)/build-aux/git-log-fix	\
	    --srcdir=$(srcdir)				\
	    --since=$(gen_start_date) > $$cl.tmp &&	\
	  mv -f $$cl.tmp $$cl;				\
	fi
